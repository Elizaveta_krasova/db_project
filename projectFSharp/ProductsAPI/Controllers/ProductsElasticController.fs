namespace FSharpApiSample.Controllers
//
// open System
// open Microsoft.AspNetCore.Mvc
// open Microsoft.Extensions.Logging
// open Nest
//
// type FileData = {
//   Id: string;
//   Directoryname: string;
//   Filename: string;
//   Filetype: string;
//   Contents: string;
//   CreateDate: DateTime;
//   ModifyDate: DateTime;
//   IndexDate: DateTime
//
// }
//
// // let inline nullable (a:int) :Nullable<int> = System.Nullable<int>(a)
// //
// // let hashAlgorithm= new SHA1Managed()
// //
// //     
// // let hash (s:string) =
// //     let bytes = Encoding.Unicode.GetBytes(s)
// //     hashAlgorithm.ComputeHash(bytes)
// //     |> Array.map (fun x -> String.Format("{0:x2}", x))
// //     |> String.concat ""
// //
//
// [<ApiController>]
// [<Route("api/products/elastic")>]
// type ProductsElasticController(logger: ILogger<ProductsElasticController>) =
//     inherit ControllerBase()
//     
//     let settings = new ConnectionSettings(new Uri("https://localhost:41554"))
//     let client = new ElasticClient(settings);
//     
//     
//     [<HttpGet>]
//     member x.GetProductsElastic() = task {
//         let text = "name1"
//         let result = client.Search<FileData>(fun (s:SearchDescriptor<FileData>) ->
//             new SearchRequest(
//                 Query = new QueryContainer(query = new TermQuery(Field = new Field("filename"), Value = text)
//             )
//         ) :> ISearchRequest)
//         printfn $"{result.Documents}"
//         return 
//     }
