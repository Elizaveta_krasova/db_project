namespace FSharpApiSample.Services

open System
open System.Data
open System.Text.Json
open System.Text.Json.Nodes
open Microsoft.Extensions.Logging
open Dapper
open Npgsql.Internal.TypeHandlers

[<CLIMutable>]
type Suppliers = {
    id: int
    name: string
    city: string[]
}

type SuppliersRepository(logger: ILogger<SuppliersRepository>, pgConnection :IDbConnection) =
    
    member _.GetSuppliersByCity(city: string) = task {
        try
            let parameters = dict ["city", box city]
            let query = $"""
select * from Suppliers
where city && array [@city];
"""
            return! pgConnection.QueryAsync<Suppliers>(query, parameters)
        with
        | err ->
            logger.LogError("GetSuppliersByCity: {err}", err)
            return failwith err.Message
    }
