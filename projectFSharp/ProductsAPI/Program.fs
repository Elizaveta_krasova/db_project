namespace FSharpApiSample

open System
open System.Data
open System.Net.Http
open ClickHouse.Client
open ClickHouse.Client.ADO
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Npgsql
open StackExchange.Redis
open FSharpApiSample.Services
open NamedServices.Microsoft.Extensions.DependencyInjection

module Program =
    DotNetEnv.Env.Load() |> ignore
    
    let private pgConnectionString = $"""
Server={DotNetEnv.Env.GetString("POSTGRES_HOST", "127.0.0.1")};
Port={DotNetEnv.Env.GetString "POSTGRES_PORT"};
Database={DotNetEnv.Env.GetString "POSTGRES_DB"};
User Id={DotNetEnv.Env.GetString "POSTGRES_USER"};
Password={DotNetEnv.Env.GetString "POSTGRES_PASSWORD"};
"""

    let private replicaPgConnectionString = $"""
Server={DotNetEnv.Env.GetString"SLAVE_HOST"};
Port={DotNetEnv.Env.GetString "SLAVE_PORT"};
Database={DotNetEnv.Env.GetString "POSTGRES_DB"};
User Id={DotNetEnv.Env.GetString "POSTGRES_USER"};
Password={DotNetEnv.Env.GetString "POSTGRES_PASSWORD"};
"""

    let private redisConnectionString = $"""
{DotNetEnv.Env.GetString("REDIS_HOST", "127.0.0.1")}:{DotNetEnv.Env.GetString "REDIS_PORT"},
password={DotNetEnv.Env.GetString "REDIS_PASSWORD"}
"""

    let private elasticConnectionString = $"""http://{DotNetEnv.Env.GetString("ELASTIC_HOST", "127.0.0.1")}:{DotNetEnv.Env.GetString "ELASTIC_PORT"}"""

    let private clickhouseConnectionString = $"""
Host={DotNetEnv.Env.GetString("CLICKHOUSE_HOST", "127.0.0.1")};
Port={DotNetEnv.Env.GetString "CLICKHOUSE_PORT"};
Database=postgres_repl;
Username={DotNetEnv.Env.GetString "CLICKHOUSE_USER"};
Password={DotNetEnv.Env.GetString "CLICKHOUSE_PASSWORD"};
"""

    [<EntryPoint>]
    let main args =
        let builder = WebApplication.CreateBuilder args

        builder.Services.AddControllers() |> ignore
        builder.Services.AddEndpointsApiExplorer() |> ignore
        builder.Services.AddSwaggerGen() |> ignore
        
        builder.Services.AddSpaStaticFiles(fun conf -> conf.RootPath <- "wwwroot")

        let frontendDevPolicy = "frontend_dev_policy"
        builder.Services.AddCors(fun options ->
            options.AddPolicy(frontendDevPolicy, fun policyBuilder ->
                policyBuilder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader() |> ignore)) |> ignore

        builder.Services.AddScoped<IDbConnection>(fun _ -> new NpgsqlConnection(pgConnectionString)) |>ignore

        builder.Services.AddNamedScoped<IDbConnection>("replica", fun _ ->
            new NpgsqlConnection(replicaPgConnectionString) :> IDbConnection) |>ignore

        let elasticClient = new HttpClient()
        elasticClient.BaseAddress <- Uri(elasticConnectionString)
        builder.Services.AddSingleton(elasticClient) |> ignore

        builder.Services.AddSingleton<IClickHouseConnection>(
            new ClickHouseConnection(clickhouseConnectionString)) |> ignore

        builder.Services.AddScoped<RegularCustomersRepository>() |> ignore
        builder.Services.AddScoped<ProductsRepository>() |> ignore
        builder.Services.AddScoped<CustomersPurchasesRepository>() |> ignore
        builder.Services.AddScoped<EmployeesRepository>() |> ignore
        builder.Services.AddScoped<SuppliersRepository>() |> ignore
        builder.Services.AddSingleton(ConnectionMultiplexer.Connect(redisConnectionString).GetDatabase()) |> ignore
        
        
        let app = builder.Build()
        
        if app.Environment.IsDevelopment() then
            app.UseCors(frontendDevPolicy) |> ignore

        app.UseStaticFiles() |> ignore
        app.UseRouting() |> ignore
        app.UseEndpoints(fun builder -> builder.MapControllers() |> ignore) |> ignore

        app.UseSwagger() |> ignore
        app.UseSwaggerUI() |> ignore

        app.Map("", fun (applicationBuilder: IApplicationBuilder) ->
            applicationBuilder.UseSpa(fun spaBuilder -> spaBuilder.Options.SourcePath <- "/wwwroot")) |> ignore

        app.Run()

        0
