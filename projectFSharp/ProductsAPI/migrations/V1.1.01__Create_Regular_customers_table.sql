create table Regular_customers
(
    id int generated always as identity primary key,
    first_name text not null,
    last_name text not null,
    patronymic text
);