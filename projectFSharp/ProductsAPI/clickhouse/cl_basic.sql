drop table if exists employees;

create table employees
(
    id int,
    first_name text,
    last_name text,
    patronymic text,
    "education.name"            Array(text),
    "education.completion_year" Array(int),
    "education.degree"          Array(text)
) engine = ReplacingMergeTree() order by (id);

insert into employees (id, first_name, last_name, patronymic, "education.name", "education.completion_year", "education.degree")
values (1, 'name1', 'name1', 'name1', ['СПбГУ', 'МГУ'], [2015, 2020], ['bachelor', 'magister']),
       (2, 'name2', 'name2', 'name2', ['АГУ', 'МГУ'], [2016, 2021], ['bachelor', 'magister']),
       (3, 'name3', 'name3', 'name3', ['НГУ', 'НГТУ'], [2014, 2021], ['bachelor', 'magister']);

-- Поиск по вложенным объектам:
select *
from employees
where has("education.name", 'НГУ')

-- Применение агрегаций к вложенным объектам:
select avg("education.completion_year")
from employees
         array join "education.completion_year";

-- Обновление данных (вставка с существующим ключом):
insert into employees (id, first_name, last_name, patronymic, "education.name", "education.completion_year", "education.degree")
values (1, 'name1', 'name1', 'name1', ['СПбГУ'], [2015], ['bachelor']);

-- Вывод последних версий строк из таблицы:
select * from employees final;