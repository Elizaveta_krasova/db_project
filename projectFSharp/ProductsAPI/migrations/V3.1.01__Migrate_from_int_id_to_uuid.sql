begin;

create extension if not exists "uuid-ossp";

-- -- Для таблицы Products_to_shops удаляем constraint с внешним ключом
alter table Products_to_shops
drop constraint Products_to_shops_shop_id_fkey;

alter table Products_to_shops
    rename column shop_id to old_shop_id;

alter table Products_to_shops
    add column shop_id uuid;


-- Для таблицы Purchases удаляем constraint с внешним ключом
alter table Purchases
drop constraint Purchases_shop_id_fkey;

alter table Purchases
    rename column shop_id to old_shop_id;

alter table Purchases
    add column shop_id uuid;

-- Для таблицы Employees удаляем constraint с внешним ключом
alter table Employees
drop constraint Employees_shop_id_fkey;

alter table Employees
    rename column shop_id to old_shop_id;

alter table Employees
    add column shop_id uuid;


-- в таблице Shops удаляем constraint primary key
alter table Shops
drop constraint Shops_pkey;

-- переименовываем колонку со старым ключом
alter table Shops
    rename column id to old_id;

-- добавляем колонку с новым ключом
alter table Shops
    add column id uuid default uuid_generate_v4();


/*
анонимная функция, которая циклом обходит все строки таблицы Shops,
для каждой строки добавляет новый id, и всем ссылающимся на эту строку
строкам из других таблиц проставляет новый id
*/
do
$$
    declare
row record;
begin
for row in select * from Shops
                             loop
update Products_to_shops set shop_id = row.id where old_shop_id = row.old_id;
update Purchases set shop_id = row.id where old_shop_id = row.old_id;
update Employees set shop_id = row.id where old_shop_id = row.old_id;
end loop;
end
$$;


-- удаляем столбец со старым id
alter table Shops
    drop column old_id;
-- устанавливаем первичный ключ
alter table Shops
    add primary key (id);


-- удаляем столбец со старым внешним ключом
alter table Products_to_shops
drop column old_shop_id;
   
-- устанавливаем новый внешний ключ
alter table Products_to_shops
    add constraint fk_shop_id foreign key (shop_id) references Shops;

-- для связи many-to-many ставим соответствующие ограничения
alter table Products_to_shops
    alter column shop_id set not null;

alter table Products_to_shops
    add primary key (shop_id, product_id);

-- удаляем столбец со старым внешним ключом
alter table Purchases
drop column old_shop_id;

-- устанавливаем новый внешний ключ   
alter table Purchases
    add constraint fk_shop_id foreign key (shop_id) references Shops;

alter table Purchases
    alter column shop_id set not null;


-- удаляем столбец со старым внешним ключом
alter table Employees
drop column old_shop_id;

-- устанавливаем новый внешний ключ   
alter table Employees
    add constraint fk_shop_id foreign key (shop_id) references Shops;

alter table Employees
    alter column shop_id set not null;


commit;