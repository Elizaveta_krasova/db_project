create function refresh_customers_purchases()
    returns trigger as
    $$
begin
    refresh materialized view concurrently customers_purchases;
    return new;
end;
$$
language 'plpgsql';

create trigger update_regular_customers_table
    after insert or update or delete
    on regular_customers
    for each row
execute function refresh_customers_purchases();
