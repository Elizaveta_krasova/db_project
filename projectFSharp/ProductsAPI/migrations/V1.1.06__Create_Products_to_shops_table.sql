create table Products_to_shops
(
    product_id int not null references Products,
    shop_id int not null references Shops,
    primary key (product_id, shop_id)
)