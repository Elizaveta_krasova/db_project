create table Purchases_to_products
(
    purchase_id int not null references Purchases,
    product_id int not null references Products,
    primary key (purchase_id, product_id)
);