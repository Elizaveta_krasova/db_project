namespace FSharpApiSample.Services

open System.Data
open Microsoft.Extensions.Logging
open Dapper
open Microsoft.AspNetCore.Mvc

[<CLIMutable>]
type PgRegularCustomer = {
    id: int
    first_name: string
    last_name: string
    patronymic: string
}

type RegularCustomersRepository(logger: ILogger<RegularCustomersRepository>, pgConnection :IDbConnection) =
    
    member _.GetAllCustomers() = task {
        try
            return! pgConnection.QueryAsync<PgRegularCustomer>("Select * from regular_customers;")
        with
        | err ->
            logger.LogError("GetAllCustomers: {err}", err)
            return failwith err.Message
    }
    
    member _.GetCustomersByName(firstName: string, lastName: string, patronymic: string) = task {
        try
            let query = """
select * from regular_customers
where last_name = @lastName and first_name = @firstName and patronymic = @patronymic;
"""
            let parameters = dict [("firstName", box firstName); ("lastName", lastName); ("patronymic", patronymic)]
            return! pgConnection.QueryAsync<PgRegularCustomer>(query, parameters)
        with
        | err ->
            logger.LogError("GetCustomersByName: {err}", err)
            return failwith err.Message
    }
    
    member _.CreateRegularCustomer(firstName: string, lastName: string, patronymic: string) = task {
        try
            let query = """
insert into regular_customers (first_name, last_name, patronymic) 
values (@firstName, @lastName, @patronymic) returning id, first_name, last_name, patronymic;
"""
            let parameters = dict [("firstName", box firstName);
                                  ("lastName", lastName); ("patronymic", patronymic)]
            let! res = pgConnection.QueryAsync<PgRegularCustomer>(query, parameters)
            return Seq.head res 
        with
        | err ->
            logger.LogError("CreateRegularCustomer: {err}", err)
            return failwith err.Message
    }
    
    member _.UpdateRegularCustomer(id: int, firstName: string, lastName: string, patronymic: string) = task {
        try
            let query = """
update regular_customers 
set first_name = @firstName, last_name = @lastName, patronymic = @patronymic 
where id = @id 
returning id, first_name, last_name, patronymic;
"""
            let parameters =  dict [("firstName", box firstName); ("lastName", lastName);
                                   ("patronymic", patronymic); ("id", id)]
            let! res = pgConnection.QueryAsync<PgRegularCustomer>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("UpdateRegularCustomer: {err}", err)
            return failwith err.Message
    }
    
    member _.DeleteRegularCustomer(id: int) = task {
        try
            let query = """
delete from regular_customers 
where id = @id
returning id, first_name, last_name, patronymic;
"""
            let parameters =  dict [("id", box id)]
            let! res = pgConnection.QueryAsync<PgRegularCustomer>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("DeleteRegularCustomer: {err}", err)
            return failwith err.Message
    }