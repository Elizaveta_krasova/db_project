create index regular_customers_search_by_name on regular_customers
    using btree (last_name, first_name, patronymic);
