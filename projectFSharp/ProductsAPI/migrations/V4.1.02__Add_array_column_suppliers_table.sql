alter table Suppliers add column city text[];

update Suppliers set city = ('{Сочи, Краснодар}')
where Suppliers.name = 'Golll';

update Suppliers set city = ('{Москва, Питер}')
where Suppliers.name = 'Help';

update Suppliers set city = ('{Барнаул, Новосибирск, Казань}')
where Suppliers.name = 'Molll';

create index Supplier_city on Suppliers using gin (city);
