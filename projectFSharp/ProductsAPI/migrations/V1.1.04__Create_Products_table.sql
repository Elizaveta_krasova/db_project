create table Products
(
    id int generated always as identity primary key,
    name text not null,
    supplier_id int not null references Suppliers,
    price int not null
);
