namespace FSharpApiSample.Controllers

open ClickHouse.Client
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open FSharpApiSample.Services
open Dapper

[<CLIMutable>]
type TotalPurchasesRow = {
    supplier_name: string
    total_purchases: int64
}

[<CLIMutable>]
type PriceByRange = {
    price_from: string
    price_to: string
    count_products: string
}

[<ApiController>]
[<Route("api/suppliers")>]
type SuppliersController(logger: ILogger<SuppliersController>,
                         repository: SuppliersRepository,
                         clickhouse: IClickHouseConnection) =
    inherit ControllerBase()

    [<HttpGet>]
    member x.GetSuppliersByCity(city: string) = task {
        try
            let! suppliers = repository.GetSuppliersByCity(city)
            return suppliers |> ActionResult<seq<Suppliers>>
        with
        | err ->
            logger.LogError("GetSuppliersByCity: {err}", err)
            return x.BadRequest() |> ActionResult<seq<Suppliers>>
    }

    [<HttpGet "total_purchases">]
    member x.GetTotalPurchases() = task {
        try
            let query = """
select suppliers.name supplier_name, count() total_purchases
from products p
         join purchases_to_products pp on p.id = pp.product_id
         join suppliers s on p.supplier_id = s.id
group by supplier_id, suppliers.name
order by count() desc
"""

            let! rows = clickhouse.QueryAsync<TotalPurchasesRow>(query)
            
            return rows |> ActionResult<seq<TotalPurchasesRow>>
        with
        | err ->
            logger.LogError("GetTotalPurchases: {err}", err)
            return x.BadRequest() |> ActionResult<seq<TotalPurchasesRow>>
    }
    
    [<HttpGet "price_range">]
    member x.GetPriceByRange() = task {
        try
            let query = """
select floor(   (price) / 100) * 100 as price_from,
                (floor((price) / 100) + 1) * 100 as price_to,
                count() as count_products
            from products
group by floor((price) / 100)
order by floor((price) / 100)
"""

            let! rows = clickhouse.QueryAsync<PriceByRange>(query)
            
            return rows |> ActionResult<seq<PriceByRange>>
        with
        | err ->
            logger.LogError("GetTotalPurchases: {err}", err)
            return x.BadRequest() |> ActionResult<seq<PriceByRange>>
    }
