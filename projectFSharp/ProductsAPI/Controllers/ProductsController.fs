namespace FSharpApiSample.Controllers

open System.Net.Http
open System.Text
open System.Text.Json
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open System
open Newtonsoft.Json.Linq
open StackExchange.Redis
open FSharpApiSample.Services

type CreateProductRequest = {
    name: string
    supplier_id: int
    price: int
}

type DeleteProductRequest = {
    id: int
}

type Shop = {
    id: Guid
    name: string
}

type ESProduct = {
    id: int
    name: string
    supplier_id: int
    shops: Shop[]
}

type ESProductSearch = {
    name: string
}

type ESProductAgg = {
    max: double
}

[<ApiController>]
[<Route("api/products")>]
type ProductsController(logger: ILogger<ProductsController>,
                        redisConnection: IDatabase,
                        repository: ProductsRepository,
                        elasticClient: HttpClient) =
    inherit ControllerBase()


    [<HttpGet>]
    member x.GetProducts() = task {
        try
            let redisKey = $"/products"
            let! redisValue = redisConnection.StringGetAsync redisKey
            
            if redisValue.HasValue then
                let products = redisValue.ToString() |> JsonSerializer.Deserialize<Product[]>
                return products |> ActionResult<seq<Product>>
            
            else
                let! products = repository.GetAllProducts()
                do! redisConnection
                        .StringSetAsync(redisKey, JsonSerializer.Serialize products, TimeSpan.FromSeconds 30) :> Task

                return products |> ActionResult<seq<Product>>
        with
        | err ->
            logger.LogError("GetProducts: {err}", err)
            return x.BadRequest() |> ActionResult<seq<Product>>
    }
    
    
    [<HttpPost("create")>]
    member x.CreateProduct([<FromBody>] body : CreateProductRequest) = task {
        try
            let! result = repository.CreateProduct(body.name, body.supplier_id, body.price)        
            return result |> ActionResult<Product>
        with
        | err ->
            logger.LogError("CreateProduct: {err}", err)
            return x.BadRequest() |> ActionResult<Product>
    }

    
    [<HttpPost("update")>]
    member x.UpdateProduct([<FromBody>] body : Product) = task {
        try
            let! result = repository.UpdateProduct(body.id, body.name, body.supplier_id, body.price)         
            return result |> ActionResult<Product>
        with
        | err ->
            logger.LogError("UpdateProduct: {err}", err)
            return x.BadRequest() |> ActionResult<Product>
    }
    
    
    [<HttpDelete("delete")>]
    member x.DeleteProduct([<FromBody>] body : DeleteProductRequest) = task {
        try
            let! result = repository.DeleteProduct(body.id)             
            return result |> ActionResult<Product>
        with
        | err ->
            logger.LogError("DeleteProduct: {err}", err)
            return x.BadRequest() |> ActionResult<Product>
    }
    
    [<HttpGet "search_by_name">]
    member x.SearchByName(name: string) = task {
        try
            use body = new StringContent($"""{{"query":{{"match":{{"name":{{"query":"{name}"}}}}}}}}""", Encoding.UTF8, "application/json")
            use! resp = elasticClient.PostAsync("/products/_search", body)
            resp.EnsureSuccessStatusCode() |> ignore
            let! result = resp.Content.ReadAsStringAsync()
            let products = JToken.Parse(result).SelectTokens "hits.hits[*]._source"
                           |> Seq.map (fun token -> token.ToObject<ESProduct>())
                           |> Array.ofSeq

            return products |> ActionResult<seq<ESProduct>>
        with
        | err ->
            logger.LogError("SearchByName: {err}", err)
            return x.BadRequest() |> ActionResult<seq<ESProduct>>
    }
    
    [<HttpGet "aggregation">]
    member x.Aggregation() = task {
        try
            use body = new StringContent($"""
                                         {{
                                         "aggs":{{
                                            "by_type":{{
                                                "terms":{{
                                                    "field":"id"
                                                }},
                                                "aggs":{{
                                                    "max_price":{{
                                                        "max":{{
                                                            "field":"price"
                                                        }}
                                                    }},
                                                    "price_extended_stats":{{
                                                        "extended_stats":{{
                                                            "field":"price"
                                                        }}
                                                    }}
                                                }}
                                            }}
                                         }},
                                         "size": 0
                                         }}""", Encoding.UTF8, "application/json")
            use! resp = elasticClient.PostAsync("/products/_search", body)
            resp.EnsureSuccessStatusCode() |> ignore
            let! result = resp.Content.ReadAsStringAsync()
            let products = JToken.Parse(result).SelectTokens "aggregations.by_type.buckets[*].price_extended_stats"
                           |> Seq.map (fun token -> token.ToObject<ESProductAgg>())
                           |> Array.ofSeq

            return Seq.head products |> ActionResult<ESProductAgg>
        with
        | err ->
            logger.LogError("Aggregation: {err}", err)
            return x.BadRequest() |> ActionResult<ESProductAgg>
    }

    [<HttpGet>]
    [<Route("~/autocomplete_page")>]
    member x.GetAutocompletePage() = task {
        return x.Content(System.IO.File.ReadAllText "./wwwroot/autocomplete.html", "text/html", Encoding.UTF8)
    }    
    
    [<HttpGet "~/autocomplete">]
    member x.Autocomplete(word: string) = task {
        try
            use body = new StringContent($"""{{"query":{{"match":{{"name":{{"query":"{word}", "analyzer": "products_ngram_analyzer"}}}}}}}}""", Encoding.UTF8, "application/json")
            use! resp = elasticClient.PostAsync("/products/_search", body)
            resp.EnsureSuccessStatusCode() |> ignore
            let! result = resp.Content.ReadAsStringAsync()
            let products = JToken.Parse(result).SelectTokens "hits.hits[*]._source.name"
                           |> Seq.map (fun token -> token.ToObject<String>())
                           |> Array.ofSeq

            return products |> ActionResult<seq<String>>
        with
        | err ->
            logger.LogError("Autocomplete: {err}", err)
            return x.BadRequest() |> ActionResult<seq<String>>
    }
