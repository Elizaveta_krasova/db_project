namespace FSharpApiSample.Services

open System
open System.Data
open System.Text.Json
open System.Text.Json.Nodes
open Microsoft.Extensions.Logging
open Dapper
open Npgsql.Internal.TypeHandlers

[<CLIMutable>]
type Employees = {
    id: int
    first_name: string
    last_name: string
    patronymic: string
    shop_id: Guid
    education: string
}

type EmployeesRepository(logger: ILogger<EmployeesRepository>, pgConnection :IDbConnection) =
    
    member _.GetEmployeesByUniversityName(universityName: string) = task {
        try
            let query = $"""
select * from employees
where education @>
'[{{"name": {JsonSerializer.Serialize universityName}}}]'
"""
            return! pgConnection.QueryAsync<Employees>(query)
        with
        | err ->
            logger.LogError("GetEmployeesByUniversityName: {err}", err)
            return failwith err.Message
    }
