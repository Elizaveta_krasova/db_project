namespace FSharpApiSample.Controllers

open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open FSharpApiSample.Services

[<ApiController>]
[<Route("api/customers_purchases")>]
type CustomersPurchasesController(logger: ILogger<CustomersPurchasesController>, repository: CustomersPurchasesRepository) =
    inherit ControllerBase()

    [<HttpGet>]
    member x.GetCustomersPurchases(min_sum: int, min_purchases_count: int) = task {
        try
            let! customersPurchases = repository.GetAllCustomersPurchases(min_sum, min_purchases_count)
            return customersPurchases |> ActionResult<seq<CustomersPurchases>>
        with
        | err ->
            logger.LogError("GetCustomersPurchases: {err}", err)
            return x.BadRequest() |> ActionResult<seq<CustomersPurchases>>
    }
