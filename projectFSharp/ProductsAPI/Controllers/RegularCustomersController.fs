namespace FSharpApiSample.Controllers

open System.Text.Json
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open System
open StackExchange.Redis
open FSharpApiSample.Services

type CreateRegularCustomerRequest = {
    first_name: string
    last_name: string
    patronymic: string
}

type DeleteRegularCustomerRequest = {
    id: int
}

[<ApiController>]
[<Route("api/regular_customers")>]
type RegularCustomersController(logger: ILogger<RegularCustomersController>,
                                redisConnection: IDatabase, repository: RegularCustomersRepository) =
    inherit ControllerBase()

    [<HttpGet>]
    member x.GetRegularCustomers() = task {
        try         
            let redisKey = $"/regular_customers"
            let! redisValue = redisConnection.StringGetAsync redisKey
            
            if redisValue.HasValue then
                let result = redisValue.ToString() |>JsonSerializer.Deserialize<PgRegularCustomer[]>
                return result |> ActionResult<seq<PgRegularCustomer>>           
            else
                let! result = repository.GetAllCustomers()
                do! redisConnection
                        .StringSetAsync(redisKey, JsonSerializer.Serialize result, TimeSpan.FromSeconds 30) :> Task
    
                return result |> ActionResult<seq<PgRegularCustomer>>
        with
        | err ->
            logger.LogError("GetRegularCustomers: {err}", err)
            return x.BadRequest() |> ActionResult<seq<PgRegularCustomer>>
    }

        [<HttpGet("byParameters")>]
    member x.GetProductsByName(first_name: string, last_name: string, patronymic: string) = task {
        try
            let! result = repository.GetCustomersByName(first_name, last_name, patronymic)
            return result |> ActionResult<seq<PgRegularCustomer>>
        with
        | err ->
            logger.LogError("GetCustomersByName: {err}", err)
            return x.BadRequest() |> ActionResult<seq<PgRegularCustomer>>
    }
    
    [<HttpPost("create")>]
    member x.CreateRegularCustomer([<FromBody>] body : CreateRegularCustomerRequest) = task {
        try
            let! result = repository.CreateRegularCustomer(body.first_name, body.last_name, body.patronymic)
            return result |> ActionResult<PgRegularCustomer>
        with
        | err ->
            logger.LogError("CreateRegularCustomer: {err}", err)
            return x.BadRequest() |> ActionResult<PgRegularCustomer>
    }
    
    [<HttpPost("update")>]
    member x.UpdateRegularCustomer([<FromBody>] body : PgRegularCustomer) = task {
        try
            let! result = repository.UpdateRegularCustomer(body.id, body.first_name, body.last_name, body.patronymic)            
            return result |> ActionResult<PgRegularCustomer>
        with
        | err ->
            logger.LogError("UpdateRegularCustomer: {err}", err)
            return x.BadRequest() |> ActionResult<PgRegularCustomer>
    }
    
    [<HttpDelete("delete")>]
    member x.DeleteRegularCustomer([<FromBody>] body : DeleteRegularCustomerRequest) = task {
        try
            let! result = repository.DeleteRegularCustomer(body.id) 
            return result |> ActionResult<PgRegularCustomer>
        with
        | err ->
            logger.LogError("DeleteRegularCustomer: {err}", err)
            return x.BadRequest() |> ActionResult<PgRegularCustomer>
    }