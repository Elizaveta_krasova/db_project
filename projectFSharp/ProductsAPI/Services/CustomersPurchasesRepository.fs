namespace FSharpApiSample.Services

open System
open System.Data
open Microsoft.Extensions.Logging
open Dapper
open NamedServices.Microsoft.Extensions.DependencyInjection

[<CLIMutable>]
type CustomersPurchases = {
    id: int
    first_name: string
    last_name: string
    patronymic: string
    min_purchases_count: int
    min_sum: int
}

type CustomersPurchasesRepository(logger: ILogger<CustomersPurchasesRepository>, serviceProvider: IServiceProvider) =
    
    member _.GetAllCustomersPurchases(minSum: int, minPurchasesCount: int) = task {
        try
            let replicaPgConnection = serviceProvider.GetRequiredNamedService<IDbConnection>("replica")
            let parameters = dict [("minSum", box minSum); ("minPurchasesCount", minPurchasesCount)]
            let query = """
select * from customers_purchases
where min_sum >= @minSum and min_purchases_count >= @minPurchasesCount
"""
            return! replicaPgConnection.QueryAsync<CustomersPurchases>(query, parameters)
        with
        | err ->
            logger.LogError("GetCustomersPurchases: {err}", err)
            return failwith err.Message
    }
